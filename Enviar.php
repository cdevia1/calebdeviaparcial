<?php
    error_reporting(E_ALL);
    require_once('Conexion.php');

    $conexionA = new conexionP();
    $conexion = $conexionA->conectar();
    

    if(isset($_POST['enviar'])){
    $nombre=$_POST['nombre'];
    $codigo_dane=$_POST['Codigodane'];
    $tipo_gobierno=$_POST['Tipodegobierno'];
    $num_poblacio=$_POST['Numerodepoblacion'];
    $moneda=$_POST['Moneda'];
    $num_Departamentos=$_POST['Numerodedepartamentos'];
    $sectorEconomico=$_POST['Sectoreconomico'];
  
        try {

            $conexion->beginTransaction();
            $sql = $conexion->prepare("INSERT INTO paises(nombre, codigo_dane, tipo_gobierno, num_poblacio, moneda, num_Departamentos, sectorEconomico) VALUES (:nombre, :codigo_dane, :tipo_gobierno, :num_poblacio, :moneda, :num_Departamentos, :sectorEconomico)
            ");
            $sql->bindParam(':nombre',$nombre);
            $sql->bindParam(':codigo_dane',$codigo_dane);
            $sql->bindParam(':tipo_gobierno',$tipo_gobierno);
            $sql->bindParam(':num_poblacio',$num_poblacio);
            $sql->bindParam(':moneda', $moneda);
            $sql->bindParam(':num_Departamentos',$num_Departamentos);
            $sql->bindParam(':sectorEconomico',$sectorEconomico);
            $sql->execute();

            $conexion->commit();
            echo "Se guardo los datos exitosamente";
            

        } catch (\PDOException $th) {
            $conexion->rollBack();
            echo "No se pudo guardar los datos :/ . Error: $th";
            
        }
    
    }

    function paises(){
        $conexionA = new conexionP();
        $conexion = $conexionA->conectar();
      

        $sql = $conexion->prepare("SELECT * FROM paises");
        $sql->execute();
        return $datos = $sql->fetchAll();
    }